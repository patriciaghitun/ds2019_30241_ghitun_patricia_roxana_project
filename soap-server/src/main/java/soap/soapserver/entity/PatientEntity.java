package soap.soapserver.entity;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "patient")
public class PatientEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "medical_record", length = 200)
    private String medicalRecord;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private UserEntity user; // informatii despre userul respectiv

    @ManyToOne//many patients to one caregiver
    @JoinColumn(name = "caregiver_id")
    private UserEntity caregiver;

    //Un doctor poate avea mai multi pacienti

    //@ManyToOne(cascade = CascadeType.REMOVE) //daca pun cascade type cu remove
    @ManyToOne
    // atunci inseamna ca doctorii trebuie sa fie deja in baza de date adaug un patient
    @JoinColumn(name = "doctor_id")
    private UserEntity doctor;

    public PatientEntity() {}

    public PatientEntity(Integer id, String medicalRecord, @NotNull UserEntity user, UserEntity caregiver, UserEntity doctor) {
        this.id = id;
        this.medicalRecord = medicalRecord;
        this.user = user;
        this.caregiver = caregiver;
        this.doctor = doctor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public UserEntity getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(UserEntity caregiver) {
        this.caregiver = caregiver;
    }

    public UserEntity getDoctor() {
        return doctor;
    }

    public void setDoctor(UserEntity doctor) {
        this.doctor = doctor;
    }
}