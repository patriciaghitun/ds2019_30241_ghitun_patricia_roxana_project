package soap.soapserver.endpoint;

import io.spring.guides.gs_producing_web_service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import soap.soapserver.entity.ActivityEntity;
import soap.soapserver.entity.MedicationPlanEntity;
import soap.soapserver.repository.ActivityRepository;
import soap.soapserver.repository.MedicationPlanRepository;

import java.util.ArrayList;
import java.util.List;

@Endpoint
public class MedicationPlanEndpoint {

    private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

    private MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public MedicationPlanEndpoint(MedicationPlanRepository medicationPlanRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getMedicationPlanRequest")
    @ResponsePayload
    public GetMedicationPlanResponse getMedicationPlanByPatient(@RequestPayload GetMedicationPlanRequest request) {

        System.out.println("GET MED PLANS BY PATIENT ID" + request.getPatientId());

        GetMedicationPlanResponse response = new GetMedicationPlanResponse();
        List<MedicationPlan> medicationPlansForResponse = new ArrayList<>();
        List<MedicationPlanEntity> medicationPlanEntities;

        medicationPlanEntities = medicationPlanRepository.getAllByPatientId(request.getPatientId());

        for(MedicationPlanEntity medicationPlanEntity: medicationPlanEntities) {

            MedicationPlan medicationPlan = new MedicationPlan();

            medicationPlan.setId(medicationPlanEntity.getId());
            medicationPlan.setPatientId(medicationPlanEntity.getPatient().getId());
            medicationPlan.setStart(medicationPlanEntity.getStart());
            medicationPlan.setEnd(medicationPlanEntity.getEnd());

            System.out.println("MedPlan PT RASPUNS: " + medicationPlanEntity.getId());
            medicationPlansForResponse.add(medicationPlan);
        }
        //response.setMedicationPlan(medicationPlansForResponse);
        return response;
    }

}
