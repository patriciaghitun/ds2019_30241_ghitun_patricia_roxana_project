package soap.soapserver.endpoint;
//

import io.spring.guides.gs_producing_web_service.Activity;
import io.spring.guides.gs_producing_web_service.GetActivityRequest;
import io.spring.guides.gs_producing_web_service.GetActivityResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import soap.soapserver.builder.ActivityBuilder;
import soap.soapserver.dto.ActivityDTO;
import soap.soapserver.entity.ActivityEntity;
import soap.soapserver.repository.ActivityRepository;

import java.util.ArrayList;
import java.util.List;

@Endpoint
public class ActivityEndpoint {
//
    private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

    private ActivityRepository activityRepository;

    @Autowired
    public ActivityEndpoint(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getActivityRequest")
    @ResponsePayload
    public GetActivityResponse getActivitiesByPatient(@RequestPayload GetActivityRequest request) {

        System.out.println("GET ACTIVITIES BY PATIENT ID - SOAP_SERVER" + request.getId());

        GetActivityResponse response = new GetActivityResponse();
        List<Activity> activitiesForResponse = new ArrayList<>();
        List<ActivityEntity> activityEntitiesList;

        activityEntitiesList = activityRepository.findActivityEntitiesByPatientId(request.getId());

        for(ActivityEntity activityEntity: activityEntitiesList) {
            System.out.println("ACTIVITATI GASITE DIN DB - SOAP");
            System.out.println(activityEntity.getLabel());
            Activity currentActivity = new Activity();

            currentActivity.setId(activityEntity.getId());
            currentActivity.setLabel(activityEntity.getLabel());
            currentActivity.setPatientId(activityEntity.getPatientId());
            currentActivity.setStart(activityEntity.getStart());
            currentActivity.setEnd(activityEntity.getEnd());

            System.out.println("ACTIVITATE PT RASPUNS - SOAP: " + currentActivity.getLabel());
            activitiesForResponse.add(currentActivity);
        }
        //response.setActivity(activitiesForResponse);
        return response;
    }


}