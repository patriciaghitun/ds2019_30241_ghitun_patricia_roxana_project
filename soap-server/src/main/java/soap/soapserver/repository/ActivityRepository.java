package soap.soapserver.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import soap.soapserver.entity.ActivityEntity;

import java.util.List;

@Repository
public interface ActivityRepository extends JpaRepository<ActivityEntity, Integer> {
    @Query(value = "SELECT u " +
            "FROM ActivityEntity u " +
            "ORDER BY u.id")
    List<ActivityEntity> getAllOrdered(); // by id

    List<ActivityEntity> getAllByPatientIdOrderByStartAsc(Integer patientId);

    ActivityEntity findActivityEntityByLabel(String label);

    ActivityEntity findActivityEntityById(Integer id);

    List<ActivityEntity> findActivityEntitiesByPatientId(Integer patientId);
}
