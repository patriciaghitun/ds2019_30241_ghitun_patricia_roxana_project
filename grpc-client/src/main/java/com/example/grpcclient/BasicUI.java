package com.example.grpcclient;


import com.example.grpc.*;
import io.grpc.ManagedChannel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import static java.lang.Thread.sleep;

class BasicUI {

    private JFrame frame = new JFrame("Client - pill box");
    private JLabel timeLabel;

    private ManagedChannel managedChannel;
    private MessageServiceGrpc.MessageServiceBlockingStub messageStub;

    private MedicationPlanResponse medPlansList;

    public BasicUI(ManagedChannel managedChannel, MessageServiceGrpc.MessageServiceBlockingStub messageStub) {

        this.managedChannel = managedChannel;
        this.messageStub = messageStub;

        frame.setLayout(null);
        frame.getContentPane().setLayout(new GridLayout(0,1));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400,600);

        timeLabel = new JLabel("");
        timeLabel.setBounds(30, 30, 150, 30);

        //Thread separat pentru afisarea timerului
        Thread thread = new Thread(() -> {
            try {
                for(;;) {
                    LocalTime time = LocalTime.now();
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
                    System.out.println("TIMP:" + time.format(formatter));
                    timeLabel.setText("Current time: " + time.format(formatter));
                    sleep(1000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        thread.start();

        Thread thread2 = new Thread(() -> {
            try {
                for(;;) {
                    checkHourlyForMedicines(this.medPlansList);
                    sleep(1000*10);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        thread2.start();
        frame.getContentPane().add(timeLabel);

        frame.setVisible(true);
    }

    public void loadWithData(MedicationPlanResponse medicationPlanResponse) {
        int startIndex = 0;
        for(MedPlanGrpc medPlanGrpc: medicationPlanResponse.getMedicationPlanListList()) {
            addMedPlanToGUI(startIndex, medPlanGrpc);
            startIndex++;
        }
        this.medPlansList = medicationPlanResponse;
    }

    public void checkHourlyForMedicines(MedicationPlanResponse medicationPlanResponse) {
        System.out.println("MED - NOT TAKEN - HOURLY");
        Thread thread = new Thread(() -> {
            try {
                for (MedPlanGrpc medPlanGrpc : medicationPlanResponse.getMedicationPlanListList()) {
                    for (MedGrpc medGrpc : medPlanGrpc.getMedicationsListList()) {
                        if (!checkCurrentMed(medGrpc)) {
                            // mark as not taken
                            noticeMedNotTaken(medGrpc);
                        }
                    }
                }
                sleep(1000*60*2); //2 de min
            } catch (ParseException | InterruptedException e) {
                e.printStackTrace();
            }
        });
        thread.start();
    }

    public void refreshData() {
        Thread thread = new Thread(() -> {
            try {

                MedicationPlanRequest request = MedicationPlanRequest.newBuilder()
                        .setRequest("first")
                        .build();

                MedicationPlanResponse response = this.messageStub.getAllMedicationPlans(request);
                System.out.println("REFRESHED RESPONSE:" + response);

                sleep(1000*10); //5 de sec
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        thread.start();
    }

    public void addMedPlanToGUI(int startIndex, MedPlanGrpc medPlanGrpc) {
        JLabel medTitle = new JLabel("Medication Plan :" + medPlanGrpc.getMedPlanId());
        frame.add(medTitle);
        for(MedGrpc medGrpc : medPlanGrpc.getMedicationsListList()) {
            addMedtoGUI(medGrpc, medPlanGrpc.getMedPlanId());
        }
    }

    public void addMedtoGUI(MedGrpc medGrpc, Integer medPlanId) {
        System.out.println("Adding med to plan");

        JPanel medPanel = new JPanel();
        medPanel.setLayout(new FlowLayout());

         JLabel medTitle = new JLabel(medGrpc.getName());
         JLabel intakeInterval = new JLabel(medGrpc.getIntake());
         JButton takeButton = new JButton("Take");

        takeButton.addActionListener(new ActionListener() {
                                         public void actionPerformed(ActionEvent e) {
                                             try {
                                                 System.out.println("TAKE OR NOT?" + checkCurrentMed(medGrpc));
                                                 if(checkCurrentMed(medGrpc)) {
                                                     intakeInterval.setText("- taken");
                                                     takeButton.setVisible(false);
                                                     noticeMedTaken(medGrpc, medPlanId);
                                                 } else {
                                                     JOptionPane.showMessageDialog(frame, "Medicine cannot be taken now");
                                                     //noticeMedNotTaken(medGrpc);
                                                 }
                                             } catch (ParseException ex) {
                                                 ex.printStackTrace();
                                             }
                                         }
                                     }
        );



        medPanel.add(medTitle);
        medPanel.add(intakeInterval);
        medPanel.add(takeButton);
        frame.getContentPane().add(medPanel);
    }

    //PARTEA DE CONTROLLER
    public boolean checkCurrentMed(MedGrpc medGrpc) throws ParseException {
        String[] intervals = medGrpc.getIntake().split(" ");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");

        LocalTime startTime = LocalTime.parse(intervals[0],formatter);
        LocalTime endTime = LocalTime.parse(intervals[1],formatter);

        LocalTime currentTime = LocalTime.now();

        boolean take = true;
        if (startTime.isAfter(endTime)) {
            //pe timp de zi
            if (currentTime.isBefore(endTime) || currentTime.isAfter(startTime)) {
                System.out.println("TRUE");
            } else {
                take = false;
            }
        } else {
            if (currentTime.isBefore(endTime) && currentTime.isAfter(startTime)) {
                System.out.println("TRUE");
            } else {
                take = false;
            }
        }
        return take;
    }

        public void noticeMedTaken(MedGrpc medGrpc, Integer medPlanId) {

            String medIdString = String.valueOf(medGrpc.getMedId());
            String medPlanIdString = medPlanId.toString();

            String messageToSend = medIdString + "-" + medPlanIdString;

            MedStatus medStatus = MedStatus.newBuilder()
                    .setStatus(messageToSend)
                    .build();

            ServerResponse serverResponse = messageStub.noticeTaken(medStatus);
            System.out.println("Server response on med taken:"+ serverResponse.getServerResponse());
        }

    public void noticeMedNotTaken(MedGrpc medGrpc) {
        String medId = String.valueOf(medGrpc.getMedId());
        MedStatus medStatus = MedStatus.newBuilder()
                .setStatus(medId)
                .build();

        ServerResponse serverResponse = messageStub.noticeNotTaken(medStatus);
        System.out.println("Server response on med not taken:"+ serverResponse.getServerResponse());
    }



}