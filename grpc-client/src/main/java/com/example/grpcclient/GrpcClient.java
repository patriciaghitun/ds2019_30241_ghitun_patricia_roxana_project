package com.example.grpcclient;


import com.example.grpc.MedicationPlanRequest;
import com.example.grpc.MedicationPlanResponse;
import com.example.grpc.MessageServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

import java.util.logging.Level;
import java.util.logging.Logger;

public class GrpcClient {

    private static final Logger logger = Logger.getLogger(GrpcClient.class.getName());

    public static void main(String[] args) {

        System.out.println("GRPC CLIENT");
//        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost",6565).usePlaintext().build();

        //PT DOCKER:
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost",6565).usePlaintext().build();
        MessageServiceGrpc.MessageServiceBlockingStub messageStub = MessageServiceGrpc.newBlockingStub(channel);

        MedicationPlanRequest request = MedicationPlanRequest.newBuilder()
                                                        .setRequest("first")
                                                        .build();

        MedicationPlanResponse response;

        try {
            response = messageStub.getAllMedicationPlans(request);
            System.out.println("RESPONSE: "+ response);
            System.out.println("Starting gui");
            BasicUI basicUI = new BasicUI(channel, messageStub);
            basicUI.loadWithData(response);

        } catch (StatusRuntimeException e) {
            logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
            return;
        }

    }

    public static void disableWarning() {
        System.err.close();
        System.setErr(System.out);
    }
}
