package com.example.springdemo;


import com.example.consumingwebservice.wsdl.Activity;
import com.example.consumingwebservice.wsdl.GetActivityResponse;
import com.example.consumingwebservice.wsdl.GetMedicationPlanResponse;
import com.example.consumingwebservice.wsdl.MedicationPlan;
import com.example.springdemo.services.MedicationPlanService;
import com.example.springdemo.soap.ActivityClient;
import com.example.springdemo.soap.MedicationPlanClient;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.util.TimeZone;

@SpringBootApplication
public class SpringDemoApplication {


	public static void main(String[] args) throws IOException, InterruptedException {

			TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
			SpringApplication.run(SpringDemoApplication.class, args);

	}

//	@Bean
//	CommandLineRunner lookup(MedicationPlanClient quoteClient) {
//		return args -> {
//
//			Integer id = 1;
//
//			GetMedicationPlanResponse response = quoteClient.getMedicationPlansForPatient(id);
//			System.out.println("RESPONSE: ");
//			for(MedicationPlan medicationPlan: response.getMedicationPlan()) {
//				System.out.println("MED PLAN: "+ medicationPlan.getId());
//
//				int foundMedPlanId = medicationPlan.getId();
//			}
//		};
//	}
}