package com.example.springdemo.entities.person;

import org.aspectj.weaver.NameMangler;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "patient")
public class Patient implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "medical_record", length = 200)
    private String medicalRecord;

    //USER
//    @NotNull
//    @OneToOne(cascade = CascadeType.PERSIST)
//    @JoinColumn(name = "user_id")
//    private User user; // informatii despre userul respectiv
//
//    @ManyToOne//many patients to one caregiver
//    @JoinColumn(name = "caregiver_id")
//    private User caregiver;
//
//    //Un doctor poate avea mai multi pacienti
//
//    @ManyToOne(cascade = CascadeType.ALL) //daca pun cascade type cu remove
//    // atunci inseamna ca doctorii trebuie sa fie deja in baza de date adaug un patient
//    @JoinColumn(name = "doctor_id")
//    private User doctor;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user; // informatii despre userul respectiv

    @ManyToOne//many patients to one caregiver
    @JoinColumn(name = "caregiver_id")
    private User caregiver;

    //Un doctor poate avea mai multi pacienti

    //@ManyToOne(cascade = CascadeType.REMOVE) //daca pun cascade type cu remove
    @ManyToOne
    // atunci inseamna ca doctorii trebuie sa fie deja in baza de date adaug un patient
    @JoinColumn(name = "doctor_id")
    private User doctor;

    public Patient() {}

    public Patient(Integer id, String medicalRecord, @NotNull User user, User caregiver, User doctor) {
        this.id = id;
        this.medicalRecord = medicalRecord;
        this.user = user;
        this.caregiver = caregiver;
        this.doctor = doctor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(User caregiver) {
        this.caregiver = caregiver;
    }

    public User getDoctor() {
        return doctor;
    }

    public void setDoctor(User doctor) {
        this.doctor = doctor;
    }


}