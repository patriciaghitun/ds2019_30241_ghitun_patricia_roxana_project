package com.example.springdemo.dto.builders.medication;

import com.example.springdemo.dto.medication.MedicationDTO;
import com.example.springdemo.entities.medication.Medication;
public class MedicationBuilder {

    private MedicationBuilder() {
    }

    public static MedicationDTO generateDTOFromEntity(Medication medication) {
        return new MedicationDTO(
                medication.getId(),
                medication.getName(),
                medication.getSideEffects(),
                medication.getDosage());
    }

    public static Medication generateEntityFromDTO(MedicationDTO medicationDTO) {
        return new Medication(
                medicationDTO.getId(),
                medicationDTO.getName(),
                medicationDTO.getSideEffects(),
                medicationDTO.getDosage()
        );
    }
}
