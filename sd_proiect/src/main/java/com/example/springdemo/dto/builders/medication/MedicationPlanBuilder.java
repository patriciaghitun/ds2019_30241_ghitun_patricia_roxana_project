package com.example.springdemo.dto.builders.medication;

import com.example.springdemo.dto.medication.MedicationPlanDTO;
import com.example.springdemo.entities.medication.MedicationPlan;

public class MedicationPlanBuilder {

    private MedicationPlanBuilder() {
    }

    public static MedicationPlanDTO generateDTOFromEntity(MedicationPlan medicationPlan) {
        return new MedicationPlanDTO(
                medicationPlan.getId(),
                medicationPlan.getPatient(),
                medicationPlan.getStart(),
                medicationPlan.getEnd());
    }

    public static MedicationPlan generateEntityFromDTO(MedicationPlanDTO medicationPlanDTO) {
        return new MedicationPlan(
                medicationPlanDTO.getId(),
                medicationPlanDTO.getPatient(),
                medicationPlanDTO.getStart(),
                medicationPlanDTO.getEnd()
        );
    }
}
