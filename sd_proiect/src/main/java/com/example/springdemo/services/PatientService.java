package com.example.springdemo.services;

import com.example.springdemo.dto.builders.person.PatientBuilder;
import com.example.springdemo.dto.person.PatientDTO;
import com.example.springdemo.entities.person.Patient;
import com.example.springdemo.entities.person.Role;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private final PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public PatientDTO findPatientById(Integer id){
        Optional<Patient> patient  = patientRepository.findById(id);

        if (!patient.isPresent()) {
            throw new ResourceNotFoundException("Patient", "patient id", id);
        }
        return PatientBuilder.generateDTOFromEntity(patient.get());
    }

    public PatientDTO findPatientByUsername(String username){
        return PatientBuilder.generateDTOFromEntity(patientRepository.getPatientByUserUsername(username));
    }

    public List<PatientDTO> findAll(){
        List<Patient> patients = patientRepository.getAllOrdered();

        return patients.stream()
                .map(PatientBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }


    public Integer insert(PatientDTO patientDTO) {
        //Inserarea unui nou pacient -> setez rolul
        patientDTO.getUser().setRole(Role.PATIENT);
        return patientRepository
                .save(PatientBuilder.generateEntityFromDTO(patientDTO))
                .getId();
    }

    public Integer update(PatientDTO patientDTO) {

        Optional<Patient> patient = patientRepository.findById(patientDTO.getId());

        if(!patient.isPresent()){
            throw new ResourceNotFoundException("Patient", "Patient id", patientDTO.getId().toString());
        }
        return patientRepository.save(PatientBuilder.generateEntityFromDTO(patientDTO)).getId();
    }

    public void delete(PatientDTO patientDTO){
        this.patientRepository.deleteById(patientDTO.getId());
    }

    public void deleteById(Integer id) {
        this.patientRepository.deleteById(id);
    }


    public List<PatientDTO> getAllByCaregiverUsername(String username) {
        List<PatientDTO> patientDTOS = new ArrayList<>();
        for(Patient patient: patientRepository.getAllByCaregiverUsername(username)) {
            patientDTOS.add(PatientBuilder.generateDTOFromEntity(patient));
        }
        return patientDTOS;
    }

}
