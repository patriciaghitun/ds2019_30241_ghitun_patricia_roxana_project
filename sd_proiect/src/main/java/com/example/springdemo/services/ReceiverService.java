package com.example.springdemo.services;

import com.example.springdemo.dto.builders.person.ActivityBuilder;
import com.example.springdemo.dto.person.ActivityDTO;
import com.example.springdemo.dto.person.PatientDTO;
import com.example.springdemo.entities.person.Activity;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

@Component
public class ReceiverService {

    private final static String QUEUE_NAME = "activity-queue";
    private final ActivityService activityService;
    private final PatientService patientService; // ca sa pun la pacient o activitate

    @Autowired
    public ReceiverService(ActivityService activityService, PatientService patientService) {

        this.activityService = activityService;
        this.patientService = patientService;

    }

    @RabbitListener(queues = QUEUE_NAME)
    private void receiveActivityMessages() throws IOException, TimeoutException {

        List<Activity> activities = new ArrayList<>();
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Consumer : waiting for messages. To exit press CTRL+C");
        int prefetchCount = 1;
        channel.basicQos(prefetchCount);

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" [x] Received '" + message);

            ObjectMapper mapper = new ObjectMapper();
            Activity currentActivity = mapper.readValue(message, Activity.class);
            System.out.println("RECEIVED ACTIVITY: "+currentActivity.toString());

            try {
                System.out.println("DURATION = H:" +
                            ActivityBuilder.generateDTOFromEntity(currentActivity)
                                    .getDurationByHours());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            System.out.println();
            activities.add(currentActivity);

            System.out.println("Anomalous? "+ checkForAnomalous(ActivityBuilder.generateDTOFromEntity(currentActivity)));

           if(checkForAnomalous(ActivityBuilder.generateDTOFromEntity(currentActivity))) {
               currentActivity.setAnomalous(true);
           } else {
               currentActivity.setAnomalous(false);
           }
            System.out.println("Activitatea a fost adaugata in baza de date!");
            activityService.insert(ActivityBuilder.generateDTOFromEntity(currentActivity));
        };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
    }

    public Boolean checkForAnomalous(ActivityDTO activityDTO) {
        long hours = 0;
        try {
            hours = activityDTO.getDurationByHours();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if( hours > 12) {
            if(activityDTO.getLabel().equals("Leaving") || activityDTO.getLabel().equals("Sleeping")){
                return true;
            }
        } else {
            if(hours > 1){
                if(activityDTO.getLabel().equals("Toileting") ||
                        activityDTO.getLabel().equals("Showering") ||
                        activityDTO.getLabel().equals("Grooming")) {
                    return true;
                }
            }
        }
        return false;
    }
}
