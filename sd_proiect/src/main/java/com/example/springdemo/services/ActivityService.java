package com.example.springdemo.services;

import com.example.springdemo.dto.builders.person.ActivityBuilder;
import com.example.springdemo.dto.person.ActivityDTO;
import com.example.springdemo.entities.person.Activity;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ActivityService {

    private final ActivityRepository activityRepository;

    @Autowired
    public ActivityService(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    public ActivityDTO findActivityById(Integer id){
        Optional<Activity> activity  = activityRepository.findById(id);

        if (!activity.isPresent()) {
            throw new ResourceNotFoundException("Activity", "activity id", id);
        }
        return ActivityBuilder.generateDTOFromEntity(activity.get());
    }

    //CRUD

    public List<ActivityDTO> findAll(){
        List<Activity> activities = activityRepository.getAllOrdered();

        return activities.stream()
                .map(ActivityBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(ActivityDTO activityDTO) {
        return activityRepository
                .save(ActivityBuilder.generateEntityFromDTO(activityDTO))
                .getId();
    }

    public Integer update(ActivityDTO activityDTO) {

        Optional<Activity> activity = activityRepository.findById(activityDTO.getId());

        if(!activity.isPresent()){
            throw new ResourceNotFoundException("activity", "activity id", activityDTO.getId().toString());
        }
        return activityRepository.save(ActivityBuilder.generateEntityFromDTO(activityDTO)).getId();
    }

    public void deleteById(Integer id) {
        this.activityRepository.deleteById(id);
    }


    public List<ActivityDTO> getAllByPatientId(Integer patientId) {
        List<Activity> activities = activityRepository.getAllByPatientIdOrderByStartAsc(patientId);

        return activities.stream()
                .map(ActivityBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }
}
