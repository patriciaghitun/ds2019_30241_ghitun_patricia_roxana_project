package com.example.rabbitmqproject;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

@SpringBootApplication
@EnableScheduling
public class RabbitmqProjectApplication {

	public static void main(String[] args) {

		SpringApplication.run(RabbitmqProjectApplication.class, args);

	}

	// get file from classpath, resources folder
	public BufferedReader getFileFromResources(String fileName) {

		ClassLoader classLoader = getClass().getClassLoader();

		InputStream resource = classLoader.getResourceAsStream(fileName);

		if (resource == null) {
			throw new IllegalArgumentException("file is not found!");
		} else {
			return new BufferedReader(new InputStreamReader(resource));
		}

	}

}
