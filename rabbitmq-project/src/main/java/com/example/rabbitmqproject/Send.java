package com.example.rabbitmqproject;


import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeoutException;

@Component
public class Send {

    private final static String QUEUE_NAME = "activity-queue";
    private static int messageQueueNr = 0;

    ParseMessage parseMessage = new ParseMessage();
    List<String> readLines = parseMessage.getReadLines();

    public Send() throws IOException {
    }

    @Scheduled(fixedDelay = 5000) // ca sa trimit doar o data la 5 sec o activitate
    public void sendActivityMessage() throws IOException, TimeoutException, JSONException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");

        try (Connection connection = factory.newConnection();
                     Channel channel = connection.createChannel()) {


                        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
                        String currentLine = readLines.get(messageQueueNr);
                        //parsarea liniei
                        String[] parts = currentLine.split("\\s\t"); // un spatiu si un tab
//                        System.out.println("parts:" + currentLine);
                        String startString = parts[0];
                        String endString = parts[1];
                        String activityLabel = parts[2];

                        //crearea json-ului cu datele

                        Random rand = new Random();
                        int patientId = rand.nextInt(4); // 0 1 2 3
                        JSONObject json = new JSONObject();
                        json.put("patientId", patientId+1); // 1 2 3 4
                        json.put("startDate", startString);
                        json.put("endDate", endString);
                        json.put("label", activityLabel);

                        channel.basicPublish("", QUEUE_NAME, null, json.toString().getBytes());
                        System.out.println(" [x] Sent '" + json.toString());
                        messageQueueNr++;

        }
    }
}

