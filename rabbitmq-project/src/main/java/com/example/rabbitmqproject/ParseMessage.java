package com.example.rabbitmqproject;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ParseMessage {

    RabbitmqProjectApplication main = new RabbitmqProjectApplication();
    private BufferedReader file = main.getFileFromResources("Activities.txt");

    public List<String> getReadLines() throws IOException {
        List<String> readLines = new ArrayList<>();
        BufferedReader br = file;
        String line;
        while ((line = br.readLine()) != null) {
            readLines.add(line);
        }
        return readLines;
    }

}
